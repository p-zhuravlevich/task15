import { StyleSheet, Switch, Text, View, TextInput, Button, Modal, CheckBox, Pressable, Alert } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';


export default function Form() {
    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => setIsEnabled(previousState => !previousState);
    const [text, onChangeText] = useState("");
    const [isSelected1, setSelection1] = useState(false);
    const [isSelected2, setSelection2] = useState(false);
    const [isSelected3, setSelection3] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);

    return (
        <View style={styles.container}>
      <View style={styles.centeredView}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            setModalVisible(!modalVisible);
          }}
          >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <Text style={styles.modalText}>Name: {text}</Text>
              <Text style={styles.modalText}>Selected plan: 
                {isSelected1? ' Plan 1' : ''} 
                {isSelected2? ' Plan 2' : ''} 
                {isSelected3? ' Plan 3' : ''}
              </Text>
              <Text style={styles.modalText}>Subscribe: {isEnabled? ' Yes' : ' No'}</Text>

              <Pressable
                style={[styles.button, styles.buttonClose]}
                onPress={() => setModalVisible(!modalVisible)}
              >
                <Text style={styles.textStyle}>Accept</Text>
                </Pressable>
            </View>
            </View>
        </Modal>
        </View>
    <View style={styles.dataBlock}>
    <Text style={styles.text}>Enter your name:</Text>
      <TextInput
        style={styles.input}
        onChangeText={onChangeText}
        value={text}
        placeholder="Heisenberg"
      />
       <View style={styles.checkboxContainer}>
        <CheckBox
          value={isSelected1}
          name=""
          onValueChange={setSelection1}
          style={styles.checkbox}
        />
        <Text style={styles.label}>Plan 1</Text>
      </View>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={isSelected2}
          onValueChange={setSelection2}
          style={styles.checkbox}
        />
        <Text style={styles.label}>Plan 2</Text>
      </View>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={isSelected3}
          onValueChange={setSelection3}
          style={styles.checkbox}
        />
        <Text style={styles.label}>Plan 3</Text>
      </View>
      <Text style={styles.text}>Do you want to subscribe?</Text>
      <Switch style={styles.switch}
        trackColor={{ false: "#767577", true: "#81b0ff" }}
        thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={toggleSwitch}
        value={isEnabled}
      />
      <Button style={styles.button}
        title="SAY MY NAME"
        onPress={() => setModalVisible(true)}
        />
      <StatusBar style="auto" />
    </View>
      
    </View>
    )
}


const styles = StyleSheet.create({
    dataBlock:{
      flex: 7,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 0
    },
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop: 0
    },
    modalView: {
      margin: 10,
      backgroundColor: "white",
      borderRadius: 20,
      paddingTop: 20,
      paddingRight: 20,
      paddingBottom: 10,
      paddingLeft: 20,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },
    button: {
      borderRadius: 10,
      padding: 10,
      elevation: 2
    },
    buttonOpen: {
      backgroundColor: "#F194FF",
    },
    buttonClose: {
      backgroundColor: "#2196F3",
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    },
    checkboxContainer: {
      flexDirection: "row",
      marginBottom: 0,
    },
    checkbox: {
      alignSelf: "center",
    },
    label: {
      margin: 8,
    },
    switch:{
      margin: 12
    },
    input: {
      height: 40,
      margin: 12,
      borderWidth: 1,
      padding: 10,
      borderColor: 'rgb(153, 108, 48)'
    },
    container: {
      flex: 5,
      fontSize: 36,
      backgroundColor: 'rgb(0, 81, 105)',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
      fontSize: 26,
    }
  });
  